<p align="center"><a href="https://www.elastic.co" target="_blank">
    <img src="https://images.contentstack.io/v3/assets/bltefdd0b53724fa2ce/blt850b5bd506c6b3ce/5d0cfe28d8ff351753cbf2ad/logo-elastic-search-color-64.svg">
</a></p>
<p align="center"><a href="https://www.elastic.co" target="_blank">
    <img src="https://images.contentstack.io/v3/assets/bltefdd0b53724fa2ce/blt38b131256d241912/5d0cfe3a970556dd5800ebfe/logo-kibana-64-color.svg">
</a></p>

Elasticsearch is a distributed RESTful search engine built for the cloud.

# elasticsearch-quick-cluster

Simple and quick elasticsearch cluster for integration environment.

This environment offers an clustering environment for Elasticsearch (one node MASTER and one node SLAVE).

For security SSL certificats (self_signed) is generate on master node.

WARN : Kibana do not use SSL certificats.

*Do not use this containers on production environment!*

# Requirement
* OS Centos7
* Docker Engine : https://www.docker.com/
* docker-compose plugin : https://docs.docker.com/compose/
* Internet

## Technical stack
* Elasticsearch 6.7.1
* Kibana 6.7.1
* Httpd 2.4 with mod_openssl

## How to use?
### Installation
* Configure docker memory : 
// TODO :
max_mem
* Configure, build, running elasticsearch cluster and read logs of master node : 
    ```console
    chmod +x ./build.sh && ./build.sh
    ```
* Installation need some informations. Answer the questions.
* Test ElasticSearch response : 
    ```console
    curl --insecure https://elastic:MY_PASSWORD_HERE@localhost:8843
    ```

### Entrypoint
* Elasticsearch Webservice : ```https://localhost:8843```
* Kibana : ```http://localhost:5601```

## Observations
* SSL certificats : maybe you need generate valid SSL certificats (with let's encrypt for exemple)