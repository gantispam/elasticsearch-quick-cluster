FROM docker.elastic.co/elasticsearch/elasticsearch:6.7.1
LABEL maintainer="gael@sigogneau.fr"

# install httpd and let's encrypt
RUN yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install httpd mod_ssl httpd-tools && \
    yum clean all

EXPOSE 8843
# ########### constants ########### 
ENV FOLDER_PATH="/opt/ssl"
# ########### variables by docker-compose ########### 
# this environment VAR was override on docker-compose
ENV ESPASSWORD="do_not_change_here"
# ########### variables by makefile ########### 
# ## ssl cert. informations
# SSL cert - geographic country (max lenght = 2) - Exemple : FR
ENV CA_COUNTRY=
# SSL cert - geographic state - Exemple : Bretagne
ENV CA_STATE=
# SSL cert - geographic city - Exemple : Nantes
ENV CA_LOCALITY=
# SSL cert - organisme's name - Exemple : Microsoft
ENV CA_ORGA=
# SSL cert - organisme's unit - Exemple : DSI
ENV CA_CN_ES=
# SSL cert - organisme's mail - Exemple : my-mail@mail.com
ENV CA_MAIL=

# apache config
RUN rm -rf  /etc/httpd/conf.d/ssl*
ADD elasticsearch.conf /etc/httpd/conf.d/elasticsearch.conf

# create SSL certificat. WARN : this is an insecure certificats : self_signed
# maybe you need to override ${FOLDER_PATH}/ca-public-crt.pem and ${FOLDER_PATH}/server-req.pem by valid SSL certificats
RUN mkdir -p ${FOLDER_PATH} \
    # Generate CA Certificate
    && openssl genrsa 2048 > ${FOLDER_PATH}/ca-private-key.pem \
    && openssl req -new -x509 -nodes -days 3600 -key ${FOLDER_PATH}/ca-private-key.pem -out ${FOLDER_PATH}/ca-public-crt.pem -subj "/C=${CA_COUNTRY}/ST=${CA_STATE}/L=${CA_LOCALITY}/O=${CA_ORGA}/OU=elasticsearch-ca/CN=${CA_CN_ES}/emailAddress=${CA_MAIL}" \
    # Create Server Certifcate and Sign with CA
    && openssl req -newkey rsa:2048 -days 3600 -nodes -keyout ${FOLDER_PATH}/server-key.pem -out ${FOLDER_PATH}/server-req.pem -subj "/C=${CA_COUNTRY}/ST=${CA_STATE}/L=${CA_LOCALITY}/O=${CA_ORGA}/OU=elasticsearch-server/CN=${CA_CN_ES}/emailAddress=${CA_MAIL}" \
    && openssl rsa -in ${FOLDER_PATH}/server-key.pem -out ${FOLDER_PATH}/server-key.pem \
    && openssl x509 -req -in ${FOLDER_PATH}/server-req.pem -days 3600 -CA ${FOLDER_PATH}/ca-public-crt.pem -CAkey ${FOLDER_PATH}/ca-private-key.pem -set_serial 01 -out ${FOLDER_PATH}/server-cert.pem \
    # Verify the Certificate
    && openssl verify -CAfile ${FOLDER_PATH}/ca-public-crt.pem ${FOLDER_PATH}/server-cert.pem

# change entrypoint : generate security password for clients
RUN  ex -s -c '6i|htpasswd -b -c /etc/httpd/conf/.htpasswd elastic $ESPASSWORD' -c x /usr/local/bin/docker-entrypoint.sh
# change entrypoint : boot httpd
RUN  ex -s -c '7i|rm -rf /run/httpd/* /tmp/httpd*' -c x /usr/local/bin/docker-entrypoint.sh
RUN  ex -s -c '8i|nohup /usr/sbin/apachectl&' -c x /usr/local/bin/docker-entrypoint.sh

# add logger
ADD log4j2.properties /usr/share/elasticsearch/config/log4j2.properties

# CMD ["elasticsearch"]