#!/bin/bash

#
# Quick installation.
#
# @requirement : centos7 + docker + docker-compose
# @since : 09/2019
# @author : gael@sigogneau.fr
#

# CONSTANTS
ENV_FILE="./.env"
DOCKER_FILE="./Dockerfile"
DOCKER_SV_NODE1="elasticsearch-node-1"
COMPOSE_FILE="elasticsearch.yml"

configurationDocker() {
	log INFO "[START] Docker Engine configuration..."
	if grep -Fxq "vm.max_map_count=262144" /etc/sysctl.conf
	then
		log INFO "[INFO] Docker is already config."
	else
		log INFO "[INFO] Set Docker memory.."
		echo "vm.max_map_count=262144" >> /etc/sysctl.conf
	fi
	log INFO "[END] Docker Engine configuration..."
}

#
# Configure environnement.
#
configuration() {
	log INFO "[START] Configuration..."

	# SSL cert - geographic country (max lenght = 2) - Exemple : FR
	privatePrompt "Which country application is setup (maxLenght=2)?\nExemple : \"FR\""
	find ${DOCKER_FILE} -type f -exec sed -i 's/CA_COUNTRY=/CA_COUNTRY='"$RESPONSE"'/g' {} \;

	# SSL cert - geographic state - Exemple : Bretagne
	privatePrompt "Which state application is setup?\nExemple : \"Bretagne\""
	find ${DOCKER_FILE} -type f -exec sed -i 's/CA_STATE=/CA_STATE='"$RESPONSE"'/g' {} \;

	# SSL cert - geographic city - Exemple : Nantes
	privatePrompt "Which city application is setup?\nExemple : \"Vannes\""
	find ${DOCKER_FILE} -type f -exec sed -i 's/CA_LOCALITY=/CA_LOCALITY='"$RESPONSE"'/g' {} \;

	# SSL cert - organisme's name - Exemple : Microsoft
	privatePrompt "What is your compagny's name?\nExemple : \"Microsoft\""
	find ${DOCKER_FILE} -type f -exec sed -i 's/CA_ORGA=/CA_ORGA='"$RESPONSE"'/g' {} \;

	# SSL cert - organisme's unit - Exemple : DSI
	privatePrompt "Which organisation?\nExemple : \"DSI\""
	find ${DOCKER_FILE} -type f -exec sed -i 's/CA_CN_ES=/CA_CN_ES='"$RESPONSE"'/g' {} \;

	# SSL cert - organisme's mail - Exemple : my-mail@mail.com
	privatePrompt "What is your mail?\nExemple : \"my-mail@mail.com\""
	find ${DOCKER_FILE} -type f -exec sed -i 's/CA_MAIL=/CA_MAIL='"$RESPONSE"'/g' {} \;

	# HTTPD - password
	privatePromptPassword "Set your password for access on Elasticsearch instance from REST webservice:"
	find ${ENV_FILE} -type f -exec sed -i 's/ESPASSWORD=/ESPASSWORD='"$RESPONSE"'/g' {} \;

	log INFO "[END] Configuration..."
}

#
# Build environnement.
#
build() {
	log INFO "[START] Build..."
	docker-compose -f ${COMPOSE_FILE} up --build -d
	wait
	log INFO "[END] Build..."
	log INFO "Environnement is builded. Read logs in 5s..."
	sleep 5s
	docker logs -f ${DOCKER_SV_NODE1}
	log INFO "[END] Logs..."
}

#
# Display prompt box.
#
# @param QUESTION
# @return RESPONSE
#
function privatePromptPassword() {
	QUESTION=$1

	log INFO "$QUESTION"
	RESPONSE=$(whiptail --title "NEED PASSWORD" --passwordbox "$QUESTION" 10 60 3>&1 1>&2 2>&3)
	exitstatus=$?
	if [ $exitstatus = 0 ]; then
		if [ "x$RESPONSE" = "x" ]; then
			log ERROR "[ERROR] password is mandatory for : $QUESTION"
			privatePrompt $QUESTION
		fi
	else
		log ERROR "[END] Installation is cancelled by user!"
		exit 1;
	fi
}

#
# Display prompt box.
#
# @param QUESTION
# @param DEFAULT_VALUE
# @return RESPONSE
#
function privatePrompt() {
	QUESTION=$1
	DEFAULT_VALUE=$2

	log INFO "$QUESTION"
	RESPONSE=$(whiptail --title "CONFIGURATION" --inputbox "$QUESTION" 10 60 $DEFAULT_VALUE 3>&1 1>&2 2>&3)
	exitstatus=$?
	if [ $exitstatus = 0 ]; then
		if [ "x$RESPONSE" = "x" ]; then
			log ERROR "[ERROR] response is mandatory for : $QUESTION"
			privatePrompt $QUESTION $DEFAULT_VALUE
		fi
	else
		log ERROR "[END] Installation is cancelled by user!"
		exit 1;
	fi
}

#
# Logger
#
# @param log_level
# @param log_message
#
function log() {
    LOG_DATE_NOW=`date '+%Y-%m-%d-%H-%M-%S'`
    # cyan
    TAG_TYPE="\033[46m[$1]\033[0m"
    if [ "x$1" = 'xERROR' ]; then
        # red
        TAG_TYPE="\033[41m[$1]\033[0m"
        >&2 echo -e "\n$TAG_TYPE - $LOG_DATE_NOW - $2"
    fi
    if [ "x$1" = 'xWARN' ]; then
        # yellow
        TAG_TYPE="\033[43m[$1]\033[0m"
    fi
    if [ "x$1" = 'xINFO' ]; then
        # blue
        TAG_TYPE="\033[44m[$1]\033[0m"
    fi
    if [ "x$1" = 'xSUCCESS' ]; then
        # green
        TAG_TYPE="\033[42m[$1]\033[0m"
    fi

    if [ "x$1" != 'xERROR' ]; then
        echo -e "\n$TAG_TYPE - $LOG_DATE_NOW - $2"
    fi
}

	#
	# Entrypoint.
	#
	echo "[START] Build elasticsearh cluster"
	configurationDocker
	configuration
	build